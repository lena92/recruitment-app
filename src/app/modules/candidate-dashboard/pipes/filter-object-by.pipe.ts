import { Pipe, PipeTransform } from '@angular/core';

/**
 * Use example: filterObjectBy:'nameOfProperty':'value';
 */
@Pipe({
  name: 'filterObjectBy'
})
export class FilterObjectByPipe implements PipeTransform {

  /**
   *
   * @param value - the array of object that needs to be filtered
   * @param args - array of arguments, 0 - property by which the array of objects is being filtered
   */
  transform(value: Array<any> | null, ...args: any[]): Array<any> | null {
    if(!value)return null;
    if(!args)return value;

    return value.filter(data => data[args[0]] === args[1]);
  }

}
