export const enum CandidateStateEnum {
  'new',
  'first_interview_scheduled',
  'technical_assessment_sent',
  'offer_given',
  'hired'
}
