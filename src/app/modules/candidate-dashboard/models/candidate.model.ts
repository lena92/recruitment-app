import {CandidateStateEnum} from "../enums/candidate-state.enum";

export interface CandidateModel {
  id: number;
  name: string;
  email: string;
  state: CandidateStateEnum;
}
