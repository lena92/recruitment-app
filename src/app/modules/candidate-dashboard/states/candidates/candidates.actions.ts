import {createAction, props} from '@ngrx/store';
import {CandidateModel} from "../../models/candidate.model";
import {CandidateStateEnum} from "../../enums/candidate-state.enum";

export enum CandidateActionsType {
  PUSH_CANDIDATE = '[Candidates Component] Push Candidate',
  UPDATE_STATE = '[Candidates Component] Update Candidate State',
}

export const pushCandidate = createAction(CandidateActionsType.PUSH_CANDIDATE, props<{candidate: CandidateModel}>());
export const updateState = createAction(CandidateActionsType.UPDATE_STATE, props<{candidateId: number, newCandidateState: CandidateStateEnum}>());
