import { createReducer, on, Action } from '@ngrx/store';
import { pushCandidate, updateState } from './candidates.actions';
import { CandidateModel } from "../../models/candidate.model";

export let initialState: Array<CandidateModel> = [
  {
    id: Math.random(),
    name: 'Lenche Petkovska',
    email: 'lencepetkovska992@gmail.com',
    state: 0
  }
];

const _candidatesReducer = createReducer(
  initialState,
  on(pushCandidate, (state, { candidate }) => {
    const items: CandidateModel[] = [{id: candidate.id, name: candidate.name, email: candidate.email, state: candidate.state}];
    state = Object.assign([], state);
    state.push(...items);
    return state;
  }),
  on(updateState, (state, { candidateId, newCandidateState }) => {
    const stateArrayCopy = Object.assign([], state);
    return stateArrayCopy.map((arrayElement: CandidateModel) => {
      if (arrayElement.id === candidateId) {
        arrayElement = {...arrayElement, state: newCandidateState};
      }
      return arrayElement;
    });
  }),
);

export function candidatesReducer(state: Array<CandidateModel> = initialState, action: Action) {
  return _candidatesReducer(state, action);
}
