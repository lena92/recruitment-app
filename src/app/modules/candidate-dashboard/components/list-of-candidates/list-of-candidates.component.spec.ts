import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfCandidatesComponent } from './list-of-candidates.component';
import {CandidateModel} from "../../models/candidate.model";
import {provideMockStore} from "@ngrx/store/testing";
import {FilterObjectByPipe} from "../../pipes/filter-object-by.pipe";

describe('ListOfCandidatesComponent', () => {
  let component: ListOfCandidatesComponent;
  let fixture: ComponentFixture<ListOfCandidatesComponent>;
  let initialState: Array<CandidateModel> = [
    {
      id: Math.random(),
      name: 'Lenche Petkovska',
      email: 'lencepetkovska992@gmail.com',
      state: 0
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListOfCandidatesComponent, FilterObjectByPipe ],
      providers: [provideMockStore({ initialState })]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfCandidatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should expect currentState to be 0', () => {
    expect(component.currentState).toEqual(0);
  });

  it('should expect currentState to be 0, 1, 2, 3 or 4', () => {
    let spy = spyOn(component, 'openState');
    console.log('spy.calls.allArgs()', spy.calls.allArgs());
    expect(spy.calls.allArgs()).toEqual([[0], [1], [2], [3], [4]]);
  });

  it('should render next state name', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(
      compiled.querySelector('div .bg-white .px-8 .pt-6 .pb-8 .mb-4 .mt-2 > dl > div > button')?.textContent
    )
      .toContain([
        'Move to "First interview scheduled" state',
        'Move to "Technical assessment sent" state',
        'Move to "Offer given" state',
        'Move to "Hired" state'
      ]);
  });
});
