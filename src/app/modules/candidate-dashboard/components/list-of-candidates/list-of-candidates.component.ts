import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {CandidateModel} from "../../models/candidate.model";
import {Observable} from "rxjs";
import {CandidateStateEnum} from "../../enums/candidate-state.enum";
import {updateState} from "../../states/candidates/candidates.actions";

@Component({
  selector: 'app-list-of-candidates',
  templateUrl: './list-of-candidates.component.html',
  styleUrls: ['./list-of-candidates.component.scss']
})
export class ListOfCandidatesComponent implements OnInit {
  candidates$: Observable<CandidateModel[]>;
  currentState: CandidateStateEnum;

  constructor(private store: Store<{ candidates: Array<CandidateModel> }>) {
    this.candidates$ = store.select('candidates');
    this.currentState = 0;
  }

  ngOnInit(): void {
  }

  openState(state: CandidateStateEnum) {
    this.currentState = state;
  }

  getNextStateName(state: CandidateStateEnum) {
    let nextState;
    switch (state) {
      case 0:
        nextState = 'First interview scheduled';
        break;
      case 1:
        nextState = 'Technical assessment sent';
        break;
      case 2:
        nextState = 'Offer given';
        break;
      case 3:
        nextState = 'Hired';
    }

    return nextState;
  }

  moveCandidateToNextState(candidateId: number, currentState: CandidateStateEnum) {
    this.store.dispatch(updateState({candidateId, newCandidateState: currentState + 1}));
  }

  candidateTrackBy(index: number, candidate: CandidateModel) {
    return candidate.id;
  }

}
