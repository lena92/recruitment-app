import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {State, Store} from "@ngrx/store";
import {CandidateModel} from "../../models/candidate.model";
import {pushCandidate} from "../../states/candidates/candidates.actions";
import {map, Observable, take} from "rxjs";

@Component({
  selector: 'app-register-candidate',
  templateUrl: './register-candidate.component.html',
  styleUrls: ['./register-candidate.component.scss']
})
export class RegisterCandidateComponent implements OnInit {
  candidateForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
  });
  successMsg: string | undefined;

  constructor(private store: Store<{ candidates: Array<CandidateModel> }>) { }

  ngOnInit(): void {
  }

  submitForm() {
    if (this.candidateForm.valid) {
      const emailExist = this.checkCandidateEmail();
      emailExist.pipe(take(1)).subscribe(result => {
        const emailControl = this.candidateForm.get('email') as FormControl;
        if (result.length <= 0) {
          this.validFormHandler();
        } else {
          emailControl.setErrors({
            notUnique: true
          });
        }
      });
    } else {
      this.notValidFormHandler();
    }
  }

  validFormHandler() {
    const candidate: CandidateModel = {
      id: Math.random(),
      name: this.candidateForm.get('name')?.value,
      email: this.candidateForm.get('email')?.value,
      state: 0
    }
    this.store.dispatch(pushCandidate({candidate}));
    this.successMsg = `Candidate ${this.candidateForm.get('name')?.value} added!`;
    this.candidateForm.reset();
    setTimeout(() => {
      this.successMsg = undefined;
    }, 1500);
  }

  notValidFormHandler() {
    for (const candidateFormKey in this.candidateForm.controls) {
      const formControl = this.candidateForm.controls[candidateFormKey] as FormControl;
      formControl.markAsDirty();
      formControl.markAsTouched();
    }
  }

  checkCandidateEmail(): Observable<CandidateModel[]> {
    return this.store.select(state => {
      return state.candidates.filter(candidate => candidate.email === this.candidateForm.get('email')?.value);
    });
  }

}
