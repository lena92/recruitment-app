import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCandidateComponent } from './register-candidate.component';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {provideMockStore} from "@ngrx/store/testing";
import {CandidateModel} from "../../models/candidate.model";

describe('RegisterCandidateComponent', () => {
  let component: RegisterCandidateComponent;
  let fixture: ComponentFixture<RegisterCandidateComponent>;
  let initialState: Array<CandidateModel> = [
    {
      id: Math.random(),
      name: 'Lenche Petkovska',
      email: 'lencepetkovska992@gmail.com',
      state: 0
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCandidateComponent ],
      providers: [provideMockStore({ initialState })]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have successMsg as undefended', () => {
    expect(component.successMsg).toEqual(undefined);
  });

  it('should expect candidateForm to be FormGroup', () => {
    const formGroup: FormGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
    });
    expect(component.candidateForm).toEqual(formGroup);
  });

  it('form invalid when empty', () => {
    expect(component.candidateForm.valid).toBeFalsy();
  });

  it('candidateForm name field validity', () => {
    let errors = {};
    let email = component.candidateForm.controls['name'];
    expect(email.errors?.['required']).toBeTruthy();
  });

  it('candidateForm email field validity', () => {
    let email = component.candidateForm.controls['email'];
    expect(email.errors?.['required']).toBeTruthy();
  });

  it('submitting a form emits a notUnique email', () => {
    expect(component.candidateForm.valid).toBeFalsy();
    component.candidateForm.controls['name'].setValue("Lenche Petkovska");
    component.candidateForm.controls['email'].setValue("lencepetkovska992@gmail.com");
    expect(component.candidateForm.valid).toBeTruthy();

    spyOn(component, 'submitForm');
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let email = component.candidateForm.controls['email'];
      expect(email.errors?.['notUnique']).toEqual(true);
    });
  });
});
