import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterCandidateComponent } from './components/register-candidate/register-candidate.component';
import { ListOfCandidatesComponent } from './components/list-of-candidates/list-of-candidates.component';
import {ReactiveFormsModule} from "@angular/forms";
import { FilterObjectByPipe } from './pipes/filter-object-by.pipe';
import {Store, StoreModule} from "@ngrx/store";
import {candidatesReducer} from "./states/candidates/candidates.reducer";


@NgModule({
  declarations: [
    RegisterCandidateComponent,
    ListOfCandidatesComponent,
    FilterObjectByPipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ candidates: candidatesReducer }, {}),
  ]
})
export class CandidateDashboardModule { }
