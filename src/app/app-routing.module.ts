import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListOfCandidatesComponent} from "./modules/candidate-dashboard/components/list-of-candidates/list-of-candidates.component";
import {RegisterCandidateComponent} from "./modules/candidate-dashboard/components/register-candidate/register-candidate.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'register-candidate',
    pathMatch: 'full',
  },
  {
    path: 'register-candidate',
    component: RegisterCandidateComponent,
  },
  {
    path: 'list-of-candidates',
    component: ListOfCandidatesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
